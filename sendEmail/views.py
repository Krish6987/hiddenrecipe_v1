from django.shortcuts import render
from uuid import uuid4
from datetime import datetime
from pymongo import MongoClient
from django.http import HttpResponseRedirect
import sendgrid

def DBConn():
    client = MongoClient("mongodb://127.0.0.1:27017")
    db = client.hidden_recipe
    return client, db

def CreateExpiryEmailHash():
    client, db = DBConn()
    dbData = db["ExpireEmailHash"].find_one({"createdIndexForKilling": "True", "forDB": "hidden_recipe"})
    if dbData == None:
        db["email_verification"].create_index("expireAt", expireAfterSeconds=86400)
        db["ExpireEmailHash"].update_one({"forDB": "hidden_recipe"}, {"$set": {"createdIndexForKilling": "True"}})
    client.close()
    return "success"

def SendVerificationEmail(email, name):
    client, db = DBConn()
    db_data = db["email_verification"].find_one({"email": email})
    if db_data == None:
        hash = str(uuid4())
        db["email_verification"].insert_one({"expireAt": datetime.utcnow(), "email": email, "hash": hash})
        client.close()
        sg = sendgrid.SendGridAPIClient(apikey='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')
        data = {
        "personalizations": [
            {
            "to": [
                {
                "email": email
                }
            ],
            "subject": "Please verify your E-mail"
            }
        ],
        "from": {
            "email": "support@hiddenrecipe.in",
            "name": "Hidden Recipe"
        },
        "content": [
            {
            "type": "text/html",
            "value": "<html><body><h3>Dear {}, Please Verify your E-mail</h3><a href='https://www.hiddenrecipe.in/email/verify/?hash={}&email={}'>click here</a> to verify your mail</body></html>".format(name, hash, email)
            }
        ]
        }
        response = sg.client.mail.send.post(request_body=data)
        CreateExpiryEmailHash()
        return response
    else:
        client.close()
        hash = db_data["hash"]
        sg = sendgrid.SendGridAPIClient(apikey='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')
        data = {
        "personalizations": [
            {
            "to": [
                {
                "email": email
                }
            ],
            "subject": "Please verify your E-mail"
            }
        ],
        "from": {
            "email": "support@hiddenrecipe.in",
            "name": "Hidden Recipe"
        },
        "content": [
            {
            "type": "text/html",
            "value": "<html><body><h3>Dear {}, Please Verify your E-mail</h3><a href='https://www.hiddenrecipe.in/email/verify/?hash={}&email={}'>click here</a> to verify your mail</body></html>".format(name, hash, email)
            }
        ]
        }
        response = sg.client.mail.send.post(request_body=data)
        CreateExpiryEmailHash()
        return response

def VerifyEmailHash(request):
    if request.method == "GET":
        if request.GET.get("hash") and request.GET.get("email"):
            hash = request.GET.get("hash")
            email = request.GET.get("email")
            client, db = DBConn()
            db_data = db["email_verification"].find_one({"email": email, "hash": hash})
            if db_data != None:
                db["users"].update_one({"email": email}, {"$set": {"email_verified": "True"}})
                client.close()
                redirect_url = "/user/signin/"
                return HttpResponseRedirect(redirect_url)
            else:
                client.close()
                redirect_url = "/frontpage/message/?message=Invalid verification link. Try logging in."
                return HttpResponseRedirect(redirect_url)
        else:
            return HttpResponseRedirect("/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")


# Create your views here.
