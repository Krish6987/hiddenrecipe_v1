from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from uuid import uuid4
from pymongo import MongoClient

def DBConn():
    client = MongoClient("mongodb://127.0.0.1:27017")
    db = client.hidden_recipe
    return client, db

def DBCreate(collection, insert_data):
    client, db = DBConn()
    coll = db[collection]
    coll.insert_one(insert_data)
    client.close()
    return "success"

def DBRead(collection, filter):
    client, db = DBConn()
    coll = db[collection]
    data = coll.find_one(filter)
    client.close()
    return data

def DBUpdate(collection, filter, data):
    client, db = DBConn()
    coll = db[collection]
    coll.update_one(filter, data)
    client.close()
    return "success"

def DBCount(collection, filter):
    client, db = DBConn()
    coll = db[collection]
    count = coll.find(filter).count()
    client.close()
    return count

def RequestCookie(request):
    if request.method == "GET":
        if request.session.get("request_cookie") == "HiddenCooKIE":
            return HttpResponseRedirect("/hidden_admin/signin/")
        else:
            request.session["request_cookie"] = "HiddenCooKIE"
            return HttpResponseRedirect("/hidden_admin/signin/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

# Create your views here.
def AdminSignin(request):
    if request.session.get("request_cookie") == "HiddenCooKIE":
        if request.method == "GET":
            if request.session.get("sid"):
                if request.session.get("type") == "hidden_recipe_admin":
                    return HttpResponseRedirect('/hidden_admin/dashboard/')
                else:
                    return HttpResponseRedirect("/user/dashboard/")
            else:
                return render(request, "hidden_admin/admin_signin.html")
        elif request.method == "POST":
            if request.session.get("sid"):
                if request.session.get("type") == "hidden_recipe_admin":
                    return HttpResponseRedirect('/hidden_admin/dashboard/')
                else:
                    return HttpResponseRedirect("/user/dashboard/")
            else:
                email = request.POST["email"]
                password = request.POST["password"]
                if email == "admin_hiddenrecipe@hiddenrecipe.in":
                    if password == "AdMinKrish@hiddenRecipe":
                        session_id = str(uuid4())[20:-1].replace("-","{]")
                        print(len(session_id))
                        print(session_id)
                        request.session["sid"] = session_id
                        request.session["type"] = "hidden_recipe_admin"
                        return HttpResponseRedirect("/hidden_admin/dashboard/")
                    else:
                        return render(request, "hidden_admin/admin_signin.html", {"message": "Incorrect Password!!"})
                else:
                    return render(request, "hidden_admin/admin_signin.html", {"message": "Invalid Credentials!!"})
        else:
            return HttpResponseRedirect("https://www.youtube.com/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def AdminDashboard(request):
    if request.session.get("request_cookie") == "HiddenCooKIE":
        if request.method == "GET":
            if request.session.get("sid"):
                if request.session.get("type") == "hidden_recipe_admin":
                    recipes_count = DBCount("recipes", {})
                    users_count = DBCount("users", {})
                    contacts = DBCount("newsletter", {})
                    reported_recipes = DBCount("reported_recipes", {})
                    return render(request, "hidden_admin/admin_dashboard.html", {"recipes_count": recipes_count, "users_count": users_count, "contacts": contacts})
                else:
                    del request.session["sid"]
                    if request.session.get("type"):
                        del request.session["type"]
                        del request.session["request_cookie"]
                    request.session.flush()
                    return HttpResponseRedirect("/")
            else:
                return HttpResponseRedirect("/user/signin/")
        else:
            return HttpResponseRedirect("https://www.youtube.com/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def AdminLogout(request):
    if request.session.get("request_cookie") == "HiddenCooKIE":
        if request.method == "GET":
            if request.session.get("sid"):
                if request.session.get("type") == "hidden_recipe_admin":
                    del request.session["type"]
                    del request.session["sid"]
                    del request.session["request_cookie"]
                    request.session.flush()
                    return HttpResponseRedirect("/")
                else:
                    del request.session["sid"]
                    request.session.flush()
                    return HttpResponseRedirect("/user/signin/")
            else:
                return HttpResponseRedirect("/user/signin/")
        else:
            return HttpResponseRedirect("https://www.youtube.com/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def ViewUsers(request):
    if request.session.get("request_cookie") == "HiddenCooKIE":
        if request.method == "GET":
            if request.session.get("sid"):
                if request.session.get("type") == "hidden_recipe_admin":
                    if request.GET.get("count"):
                        count = int(request.GET.get("count"))
                        client, db = DBConn()
                        coll = db["users"]
                        users_data_cursor = coll.find({}).skip(count).limit(5).sort([("_id", -1)])
                        total = DBCount("users",{})
                        users_data = []
                        for i in users_data_cursor:
                            users_data.append(i)
                        client.close()
                        if len(users_data) > 0:
                            return render(request, "hidden_admin/view_users.html", {"users_data": users_data, "count": count, "total": total})
                        else:
                            return render(request, "hidden_admin/view_users.html", {"users_data": "", "count": count, "total": total})
                    else:
                        return HttpResponseRedirect("/hidden_admin/dashboard/")
                else:
                    del request.session["sid"]
                    if request.session.get("type"):
                        del request.session["type"]
                    request.session.flush()
                    return HttpResponseRedirect("/")
            else:
                return HttpResponseRedirect("/user/signin/")
        else:
            return HttpResponseRedirect("https://www.youtube.com/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def ViewUser(request):
    if request.session.get("request_cookie") == "HiddenCooKIE":
        if request.method == "GET":
            if request.session.get("sid"):
                if request.session.get("type") == "hidden_recipe_admin":
                    if request.GET.get("user_id"):
                        user_id = request.GET.get("user_id")
                        user_data = DBRead("users", {"email": user_id})
                        if user_data != None:
                            client, db = DBConn()
                            coll = db["recipes"]
                            recipes_data_cursor = coll.find({"posted_by": user_id})
                            recipes_data = []
                            for i in recipes_data_cursor:
                                recipes_data.append(i)
                            client.close()
                            recipes_count = DBCount("recipes", {"posted_by": user_id})
                            return render(request, "hidden_admin/view_user.html", {"user_data": user_data, "recipes_data": recipes_data, "recipes_count": recipes_count})
                        else:
                            return HttpResponseRedirect("/hidden_admin/dashboard/")
                    else:
                        return HttpResponseRedirect("/hidden_admin/dashboard/")
                else:
                    del request.session["sid"]
                    if request.session.get("type"):
                        del request.session["type"]
                    request.session.flush()
                    return HttpResponseRedirect("/")
            else:
                return HttpResponseRedirect("/user/signin/")
        else:
            return HttpResponseRedirect("https://www.youtube.com/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def ReportedRecipes(request):
    if request.session.get("request_cookie") == "HiddenCooKIE":
        if request.method == "GET":
            if request.session.get("sid"):
                if request.session.get("type") == "hidden_recipe_admin":
                    client, db = DBConn()
                    coll = db["report_recipes"]
                    reported_recipes_cursor = coll.find().sort([("_id", -1)])
                    client.close()
                    if reported_recipes_cursor.count() != 0:
                        reported_recipes = []
                        send_data = []
                        for i in reported_recipes_cursor:
                            reported_recipes.append(i)
                        for i in reported_recipes:
                            recipe_data = DBRead("recipes", {"recipe_id": i["recipe_id"]})
                            if recipe_data != None:
                                send_data.append({
                                    "recipe_name": recipe_data["recipe_name"],
                                    "recipe_id": recipe_data["recipe_id"],
                                    "count": len(i["reason"])
                                })
                        if len(send_data) == 0:
                            send_data = ""
                            return render(request, "hidden_admin/reported_recipes.html", {"send_data": send_data})
                        else:
                            return render(request, "hidden_admin/reported_recipes.html", {"send_data": send_data})
                    else:
                        send_data = ""
                        return render(request, "hidden_admin/reported_recipes.html", {"send_data": send_data})
                else:
                    del request.session["sid"]
                    if request.session.get("type"):
                        del request.session["type"]
                    request.session.flush()
                    return HttpResponseRedirect("/")
            else:
                return HttpResponseRedirect("/user/signin/")
        else:
            return HttpResponseRedirect("https://www.youtube.com/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def ViewRecipe(request):
    if request.session.get("request_cookie") == "HiddenCooKIE":
        if request.method == "GET":
            if request.session.get("sid"):
                if request.session.get("type") == "hidden_recipe_admin":
                    if request.GET.get("recipe_id"):
                        recipe_id = request.GET.get("recipe_id")
                        recipe_data = DBRead("recipes", {"recipe_id": recipe_id})
                        if recipe_data != None:
                            reported_recipes = DBRead("report_recipes", {"recipe_id": recipe_id})
                            return render(request, "hidden_admin/view_recipe.html", {"recipe_data": recipe_data, "reported_recipes": reported_recipes})
                        else:
                            return HttpResponseRedirect("/hidden_admin/dashboard/")
                    else:
                        return HttpResponseRedirect("/hidden_admin/dashboard/")
                else:
                    del request.session["sid"]
                    if request.session.get("type"):
                        del request.session["type"]
                    request.session.flush()
                    return HttpResponseRedirect("/")
            else:
                return HttpResponseRedirect("/user/signin/")
        else:
            return HttpResponseRedirect("https://www.youtube.com/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def ViewRecipes(request):
    if request.session.get("request_cookie") == "HiddenCooKIE":
        if request.method == "GET":
            if request.session.get("sid"):
                if request.session.get("type") == "hidden_recipe_admin":
                    if request.GET.get("count"):
                        count = int(request.GET.get("count"))
                        client, db = DBConn()
                        coll = db["recipes"]
                        recipes_count = DBCount("recipes", {})
                        if count <= recipes_count:
                            recipes_data_cursor = coll.find({}).sort([("_id", -1)]).skip(count).limit(10)
                            client.close()
                            if recipes_data_cursor.count() != 0:
                                recipes_data = []
                                for i in recipes_data_cursor:
                                    user_data = DBRead("users", {"email": i["posted_by"]})
                                    i["profile_pic"] = user_data["profile_pic"]
                                    recipes_data.append(i)
                                return render(request, "hidden_admin/view_recipes.html", {"send_data": recipes_data, "count": count})
                        count = "false"
                        return render(request, "hidden_admin/view_recipes.html", {"send_data": "", count:"false"})
                    else:
                        return HttpResponseRedirect("/hidden_admin/dashboard/")
                else:
                    del request.session["sid"]
                    if request.session.get("type"):
                        del request.session["type"]
                    request.session.flush()
                    return HttpResponseRedirect("/")
            else:
                return HttpResponseRedirect("/user/signin/")
        else:
            return HttpResponseRedirect("https://www.youtube.com/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def ViewContacts(request):
    if request.session.get("request_cookie") == "HiddenCooKIE":
        if request.method == "GET":
            if request.session.get("sid"):
                if request.session.get("type") == "hidden_recipe_admin":
                    if request.GET.get("count"):
                        count = int(request.GET.get("count"))
                        client, db = DBConn()
                        coll = db["newsletter"]
                        contacts_cursor = coll.find({}).skip(count).limit(5).sort([("_id", -1)])
                        total = DBCount("users",{})
                        contacts_data = []
                        for i in contacts_cursor:
                            contacts_data.append(i)
                        client.close()
                        if len(contacts_data) > 0:
                            return render(request, "hidden_admin/view_contacts.html", {"contacts_data": contacts_data, "count": count, "total": total})
                        else:
                            return render(request, "hidden_admin/view_contacts.html", {"contacts_data": "", "count": count, "total": total})
                    else:
                        return HttpResponseRedirect("/hidden_admin/dashboard/")
                else:
                    del request.session["sid"]
                    if request.session.get("type"):
                        del request.session["type"]
                    request.session.flush()
                    return HttpResponseRedirect("/")
            else:
                return HttpResponseRedirect("/user/signin/")
        else:
            return HttpResponseRedirect("https://www.youtube.com/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")
