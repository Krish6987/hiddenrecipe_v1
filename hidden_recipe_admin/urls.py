from django.urls import path
from . import views

urlpatterns = [
    path("request-cookie/", views.RequestCookie),
    path("signin/", views.AdminSignin), 
    path("dashboard/", views.AdminDashboard),
    path("dashboard/logout/", views.AdminLogout),
    path("dashboard/view-user/", views.ViewUser),
    path("dashboard/view-users/", views.ViewUsers),
    path("dashboard/report-recipes/", views.ReportedRecipes),
    path("dashboard/view-recipe/", views.ViewRecipe),
    path("dashboard/view-recipes/", views.ViewRecipes),
    path("dashboard/view-contacts/", views.ViewContacts),
]