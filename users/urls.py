from django.urls import path
from . import views

urlpatterns = [
    path("signin/", views.LoginPage),
    path("signup/", views.RegisterPage),
    path("forgot-password/", views.ForgotPassword),
    path("dashboard/", views.UserDashboard),
    path("logout/", views.UserLogout),
    path("dashboard/post-recipe/", views.PostRecipe),
    path("dashboard/account-settings/", views.UserAccountSettings),
    path("view-profile/", views.ViewProfile),
    path("dashboard/recipe/", views.ViewRecipe),
    path("recipe/search/", views.RecipeSearch),
    path("dashboard/report/", views.ReportRecipe),
]