from django.shortcuts import render
from django.http import HttpResponse,HttpResponseRedirect
from sendEmail.views import SendVerificationEmail
from datetime import datetime
import requests, sendgrid
from pymongo import MongoClient
from uuid import uuid4
from django.core.files.storage import FileSystemStorage

def DBConn():
    client = MongoClient("mongodb://127.0.0.1:27017")
    db = client.hidden_recipe
    return client, db

def DBCreate(collection, insert_data):
    client, db = DBConn()
    coll = db[collection]
    coll.insert_one(insert_data)
    client.close()
    return "success"

def DBRead(collection, filter):
    client, db = DBConn()
    coll = db[collection]
    data = coll.find_one(filter)
    client.close()
    return data

def DBUpdate(collection, filter, data):
    client, db = DBConn()
    coll = db[collection]
    coll.update_one(filter, data)
    client.close()
    return "success"

def DBCount(collection, filter):
    client, db = DBConn()
    coll = db[collection]
    count = coll.find(filter).count()
    client.close()
    return count

def GoogleCaptcha(request):
    captcha_token = request.POST["g-recaptcha-response"]
    captcha_url = "https://www.google.com/recaptcha/api/siteverify"
    captcha_body = {
        "secret" : "6LcunKIZAAAAAJLKflX-tbAJZq0jppYiiP_VFsa1",
        "response" : captcha_token
    }
    captcha_response = requests.post(url=captcha_url, data=captcha_body).json()
    return captcha_response

def SendPassword(email):
    user_data = DBRead("users", {"email": email})
    sg = sendgrid.SendGridAPIClient(apikey='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')
    data = {
    "personalizations": [
        {
        "to": [
            {
            "email": email
            }
        ],
        "subject": "Password for Hidden Recipe"
        }
    ],
    "from": {
        "email": "support@hiddenrecipe.in",
        "name": "Hidden Recipe"
    },
    "content": [
        {
        "type": "text/html",
        "value": "<html><body><h3>Dear {}, Heres your credentials to login Hidden Recipe<br><p><b>Mailid:<b><p style='color: red'>{}</p><br><p><b>Password:<b><p style='color: red'>{}</p><br><p><a href='https://www.hiddenrecipe.in/user/signin/'>Click here</a> to login</p></body></html>".format(user_data["name"], user_data["email"], user_data["password"])
        }
    ]
    }
    response = sg.client.mail.send.post(request_body=data)
    return response

def LoginPage(request):
    if request.method == "GET":
        if request.session.get("sid"):
            return HttpResponseRedirect('/user/dashboard/')
        else:
            return render(request, "users/user_signin.html")
    elif request.method == "POST":
        if request.session.get("sid"):
            return HttpResponseRedirect('/user/dashboard/')
        else:
            email = request.POST["email"].strip()
            password = request.POST["password"]
            captcha_response = GoogleCaptcha(request)
            status = str(captcha_response["success"]).lower()
            if status == "true":
                filter_data = {
                    "email": email,
                    "password": password
                }
                data = DBRead("users", filter_data)
                if data != None:
                    if data["email_verified"] == "True":
                        if data["status"] == "inactive":
                            sg = sendgrid.SendGridAPIClient(apikey='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')
                            data = {
                            "personalizations": [
                                {
                                "to": [
                                    {
                                    "email": email
                                    }
                                ],
                                "subject": "Welcome Back {}".format(data["name"])
                                }
                            ],
                            "from": {
                                "email": "support@hiddenrecipe.in",
                                "name": "Hidden Recipe"
                            },
                            "content": [
                                {
                                "type": "text/html",
                                "value": "<html><body><h3>Dear {}, <br>Welcome back to Hidden Recipe.</h3><br> It's great to see you back in our team.<br>Search and enjoy with our service!!</body></html>".format(data["name"])
                                }
                            ]
                            }
                            response = sg.client.mail.send.post(request_body=data)
                            DBUpdate("users", {"email": email}, {"$set": {"status": "active"}})
                        session_id = str(uuid4())
                        filter = {"email": email}
                        data = {"$push": {"session_id": session_id}}
                        DBUpdate("users", filter, data)
                        request.session["sid"] = session_id
                        request.session["eid"] = email
                        return HttpResponseRedirect('/user/dashboard/')
                    else:
                        SendVerificationEmail(email, data["name"])
                        return render(request, "users/user_signin.html", {"message": "A verification link is sent to your mail. Please check your mail inbox!!"})
                else:
                    return render(request, "users/user_signin.html", {"message": "Invalid Credentials!!"})
            else:
                return HttpResponseRedirect('/user/signin/')
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def RegisterPage(request):
    if request.method == "GET":
        if request.session.get("sid"):
            return HttpResponseRedirect('/user/dashboard/')
        else:
            return render(request, "users/user_signup.html")
    elif request.method == "POST":
        if request.session.get("sid"):
            return HttpResponseRedirect('/user/dashboard/')
        else:
            name = request.POST["name"].title()
            email = request.POST["email"].lower()
            mobile = request.POST["mobile"]
            password = request.POST["password"]
            captcha_response = GoogleCaptcha(request)
            status = str(captcha_response["success"]).lower()
            if status == "true":
                data = DBRead("users", {"email": email})
                if data == None:
                    datenow = datetime.combine(datetime.utcnow().date(), datetime.min.time()) 
                    insert_data = {
                        "name": name.strip(),
                        "email": email.strip(),
                        "mobile": mobile,
                        "password": password,
                        "profile_pic": "",
                        "join_date": datenow,
                        "email_verified": "false",
                        "status": "active",
                        "rate_us": "false",
                        "session_id": []
                    }
                    DBCreate("users", insert_data)
                    SendVerificationEmail(email, name)
                    return render(request, "users/user_signup.html", {"message": "A verification is sent to your mail. Please check your inbox!"})
                else:
                    return render(request, "users/user_signup.html", {"message": "User Already Exists!!"})
            else:
                return HttpResponseRedirect('/user/signup/')
    else:
        return HttpResponseRedirect('https://www.youtube.com/')

def ForgotPassword(request):
    if request.method == "GET":
        if request.GET.get("key"):
            if request.session.get("sid"):
                if request.GET.get("key") != "true":
                    del request.session["eid"]
                    del request.session["sid"]
                    request.session.flush()
                    return HttpResponseRedirect("/user/signin/")
                else:
                    email = request.session.get("eid")
                    SendPassword(email)
                    return render(request, "users/user_account_settings.html", {"message": "Password was sent to your mail. Please check your inbox!!"})
            else:
                return HttpResponseRedirect("/user/signin/")
        else:
            return render(request, "users/user_forgot_password.html")
    elif request.method == "POST":
        email = request.POST["email"].lower().strip()
        user_data = DBRead("users", {"email": email})
        if user_data != None:
            SendPassword(email)
            return render(request, "users/user_forgot_password.html", {"message": "Password was sent to your mail. Please check your inbox!!"})
        else:
            return render(request, "users/user_forgot_password.html", {"message": "User Doesnot exist!!"})
    else:
        return HttpResponseRedirect("https://www.youtube.com/")
        
def UserDashboard(request):
    if request.method == "GET":
        if request.session.get("sid"):
            if request.session.get("type") == "hidden_recipe_admin":
                return HttpResponseRedirect("/hidden_admin/dashboard/")
            user_id = request.session.get("eid")
            user_data = DBRead("users", {"email": user_id})
            if user_data != None:
                recipes_count = DBCount("recipes", {"posted_by": user_id})
                client, db = DBConn()
                coll = db["recipes"]
                recipes_data_cursor = coll.find().sort([("_id", -1)]).limit(20)
                client.close()
                recipes_data = []
                for i in recipes_data_cursor:
                    users = DBRead("users", {"email": i["posted_by"]})
                    i["user_profile_pic"] = users["profile_pic"]
                    recipes_data.append(i)
                return render(request, "users/user_dashboard.html", {"user_data": user_data, "recipes_count": recipes_count, "recipes_data": recipes_data})
            else:
                del request.session["eid"]
                del request.session["sid"]
                request.session.flush()
                return HttpResponseRedirect('/user/signin/')
        else:
            return HttpResponseRedirect('/user/signin/')
    else:
        return HttpResponseRedirect("https://wwww.youtube.com")


def UserLogout(request):
    if request.method == "GET":
        if request.session.get("sid"):
            email = request.session.get("eid")
            session_id = request.session.get("sid")
            DBUpdate("users", {"email": email}, {"$pull": {"session_id": session_id}})
            del request.session["eid"]
            del request.session["sid"]
            request.session.flush()
            user_data = DBRead("users", {"email": email})
            if user_data["rate_us"] == "false":
                DBUpdate("users", {"email": email}, {"$set": {"rate_us": "true"}})
                return HttpResponseRedirect("https://goo.gl/forms/jgdSj7jcQ3tZ3nfl1")
            else:
                return HttpResponseRedirect("/user/signin/")
        else:
            return HttpResponseRedirect("/user/signin/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def PostRecipe(request):
    if request.method == "POST":
        if request.session.get("sid"):
            user_id = request.session.get("eid")
            user_data = DBRead("users", {"email": user_id})
            if user_data != None:
                title = request.POST["title"]
                ingredients = request.POST["ingredients"]
                ingredients_list = ingredients.split(",")
                for i in range(0,len(ingredients_list)):
                    ''.join(e for e in ingredients_list[i] if e.isalnum())
                    ingredients_list[i] = ingredients_list[i].replace(".", "")
                    ingredients_list[i] = ingredients_list[i].strip()
                description = request.POST["description"]
                date = datetime.combine(datetime.utcnow().date(), datetime.min.time()) 
                recipe_id = str(uuid4())[19:-1]
                images_list = request.FILES.getlist("images")
                images = []
                for i in images_list:
                    fs = FileSystemStorage()
                    uid = str(uuid4())
                    image_name = uid.split("-")[-1] + "." + i.name.split(".")[-1]
                    filename = fs.save(image_name, i)
                    images.append(filename)
                insert_data = {
                    "recipe_id": recipe_id,
                    "recipe_name": title.strip(),
                    "ingredients": ingredients_list,
                    "images": images,
                    "description": description,
                    "posted_by": user_id,
                    "user_name": user_data["name"],
                    "posted_date": date
                }
                DBCreate("recipes", insert_data)
                client, db = DBConn()
                coll = db["recipes"]
                coll.ensure_index([("ingredients", "text"),("recipe_name", "text")])
                client.close()
                return HttpResponseRedirect('/user/dashboard/')
            else:
                del request.session["eid"]
                del request.session["sid"]
                request.session.flush()
                return HttpResponseRedirect("/user/signin/")
        else:
            return HttpResponseRedirect("/user/signin/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def UserAccountSettings(request):
    if request.method == "GET":
        if request.session.get("sid"):
            user_id = request.session.get("eid")
            user_data = DBRead("users", {"email": user_id})
            if user_data != None:
                return render(request, "users/user_account_settings.html", {"profile_pic": user_data["profile_pic"], "session_count": len(user_data["session_id"])})
            else:
                del request.session["eid"]
                del request.session["sid"]
                request.session.flush()
                return HttpResponseRedirect("/user/signin/")
        else:
            return HttpResponseRedirect("/user/signin/")
    elif request.method == "POST":
        if request.session.get("sid"):
            flag_key = request.POST["flag_key"]
            user_id = request.session.get("eid")
            user_data = DBRead("users", {"email": user_id})
            if user_data != None:
                if flag_key == "change_password":                
                    old_password = request.POST["old-password"]
                    if user_data["password"] == old_password:
                        new_password = request.POST["new-password"]
                        DBUpdate("users", {"email": user_id}, {"$set": {"password": new_password}})
                        return render(request, "users/user_account_settings.html", {"message": "Password changed successfully!"})
                    else:
                        return render(request, "users/user_account_settings.html", {"key": "false", "old-password": old_password})
                elif flag_key == "edit_dp":
                    password = request.POST["password"]
                    if user_data["password"] == password:
                        profile_pic = request.FILES["profile-pic"]
                        fs = FileSystemStorage()
                        user_data = DBRead("users", {"email": user_id})
                        if user_data["profile_pic"]  != "":
                            fs.delete(user_data["profile_pic"])
                        uid = str(uuid4())
                        profile_pic_name = uid.split("-")[-1] + "." + profile_pic.name.split(".")[-1]
                        filename = fs.save(profile_pic_name, profile_pic)
                        DBUpdate("users", {"email": user_id}, {"$set": {"profile_pic": filename}})
                        return render(request, "users/user_account_settings.html", {"message": "Changed Profile Pic successfully!!!"})
                    else:
                        return render(request, "users/user_account_settings.html", {"message": "Incorrect Password"})
                elif flag_key == "security_login":
                    password = request.POST["password"]
                    if user_data["password"] == password:
                        if "security" in request.POST and request.POST["security"] == "on":
                            DBUpdate("users", {"email": user_id}, {"$set": {"session_id": []}})
                            del request.session["eid"]
                            del request.session["sid"]
                            request.session.flush()
                            return HttpResponseRedirect("/user/signin/")
                        else:
                            return HttpResponseRedirect("/user/dashboard/account-settings/")
                    else:
                        return render(request, "users/user_account_settings.html", {"message": "Incorrect Password"})
                elif flag_key == "deactivate":
                    password = request.POST["password"]
                    if user_data["password"] == password:
                        DBUpdate("users", {"email": user_id}, {"$set": {"status": "inactive", "session_id": []}})
                        return HttpResponseRedirect("/user/logout/")
                    else:
                        return render(request, "users/user_account_settings.html", {"message": "Incorrect Password"})
                else:
                    return HttpResponseRedirect('/user/dashboard/')
            else:
                    del request.session["eid"]
                    del request.session["sid"]
                    request.session.flush()
                    return HttpResponseRedirect("/user/signin/")
        else:
            return HttpResponseRedirect("/user/signin/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def RecipeSearch(request):
    if request.method == "POST":
        if request.session.get("sid"):
            user_id = request.session.get("eid")
            user_data = DBRead("users", {"email": user_id})
            search = request.POST["search"]
            client, db = DBConn()
            coll = db["recipes"]
            recipes_cursor = coll.find()
            client.close()
            recipes_data = []
            for i in recipes_cursor:
                recipes_data.append(i)
            ingredients_list = search.split(",")
            for i in range(0,len(ingredients_list)):
                ''.join(e for e in ingredients_list[i] if e.isalnum())
                ingredients_list[i] = ingredients_list[i].replace(".", "")
                ingredients_list[i] = ingredients_list[i].strip()
            client, db = DBConn()
            coll = db["recipes"]
            send_data = []
            recipes_count = DBCount("recipes", {})
            if recipes_count != 0:
                data1 = coll.find({"ingredients": ingredients_list})
                if data1.count() != 0:
                    for i in data1:
                        user_data = DBRead("users", {"email": i["posted_by"]})
                        i["user_profile_pic"] = user_data["profile_pic"]
                        send_data.append(i)
                for i in ingredients_list:
                    data2 = coll.find({"$text": {"$search": i}})
                    if data2.count() != 0:
                        for k in data2:
                            count = 0
                            for dict in send_data:
                                if k == dict:
                                    count = count + 1
                            if count == 0:
                                user_data = DBRead("users", {"email": k["posted_by"]})
                                k["user_profile_pic"] = user_data["profile_pic"]
                                send_data.append(k)
                client.close()
            if len(send_data) == 0:
                send_data = ""
            recipes_count = DBCount("recipes", {"posted_by": user_id})
            return render(request, "users/search_results.html", {"user_data": user_data, "recipes_count": recipes_count, "recipes_data": send_data})
        else:
            return HttpResponseRedirect("/user/signin/")

def ViewProfile(request):
    if request.method == "GET":
        if request.session.get("sid"):
            user_id = request.session.get("eid")
            user_data = DBRead("users", {"email": user_id})
            if user_data != None:
                recipes_count = DBCount("recipes", {"posted_by": user_id})
                client, db = DBConn()
                coll = db["recipes"]
                recipes_data_cursor = coll.find({"posted_by": user_id}).sort([("_id", -1)])
                client.close()
                recipes_data = []
                for i in recipes_data_cursor:
                    i["user_profile_pic"] = user_data["profile_pic"]
                    recipes_data.append(i)
                return render(request, "users/view_profile.html", {"user_data": user_data, "recipes_count": recipes_count, "recipes_data": recipes_data})
            else:
                del request.session["eid"]
                del request.session["sid"]
                request.session.flush()
                return HttpResponseRedirect('/user/signin/')
        else:
            return HttpResponseRedirect('/user/signin/')
    else:
        return HttpResponseRedirect("https://wwww.youtube.com")

def ViewRecipe(request):
    if request.method == "GET":
        if request.session.get("sid"):
            if request.GET.get("recipe_id"):
                user_id = request.session.get("eid")
                user_data = DBRead("users", {"email": user_id})
                if user_data != None:
                    recipe_id = request.GET.get("recipe_id")
                    recipe = DBRead("recipes", {"recipe_id": recipe_id})
                    if recipe != None:
                        uploaded_by = DBRead("users", {"email": recipe["posted_by"]})
                        recipe["user_profile_pic"] = uploaded_by["profile_pic"]
                        return render(request, "users/view_recipe.html", {"user_data": user_data, "recipe": recipe})
                    else:
                        return HttpResponseRedirect("/user/dashboard/")
                else:
                    del request.session["eid"]
                    del request.session["sid"]
                    request.session.flush()
                    return HttpResponseRedirect('/user/signin/')
            else:
                return HttpResponseRedirect("/user/dashboard/")
        else:
            return HttpResponseRedirect('/user/signin/')
    else:
        return HttpResponseRedirect("https://wwww.youtube.com")

def ReportRecipe(request):
    if request.method == "POST":
        if request.session.get("sid"):
            if request.GET.get("recipe_id"):
                user_id = request.session.get("eid")
                user_data = DBRead("users", {"email": user_id})
                if user_data != None:
                    recipe_id = request.GET.get("recipe_id")
                    recipe = DBRead("recipes", {"recipe_id": recipe_id})
                    if recipe != None:
                        uploaded_by = DBRead("users", {"email": recipe["posted_by"]})
                        recipe["user_profile_pic"] = uploaded_by["profile_pic"]
                        message = request.POST["report_reason"]
                        data = DBRead("report_recipes", {"recipe_id": recipe_id})
                        if data != None:
                            data = DBRead("report_recipes", {"recipe_id": recipe_id, "reported_by": user_id})
                            if data == None:
                                DBUpdate("report_recipe",{"recipe_id": recipe_id}, {"$push": {"reason": message}})
                        else:
                            client, db = DBConn()
                            coll = db["report_recipes"]
                            coll.insert({
                                "recipe_id": recipe_id,
                                "reported_by": user_id,
                                "reason": [message]
                            })
                            client.close()
                        return render(request, "users/view_recipe.html", {"user_data": user_data, "recipe": recipe, "message": "true"})
                    else:
                        return HttpResponseRedirect("/user/dashboard/")
                else:
                    del request.session["eid"]
                    del request.session["sid"]
                    request.session.flush()
                    return HttpResponseRedirect('/user/signin/')
            else:
                return HttpResponseRedirect("/user/dashboard/")
        else:
            return HttpResponseRedirect('/user/signin/')
    else:
        return HttpResponseRedirect("https://www.youtube.com/")
# Create your views here.