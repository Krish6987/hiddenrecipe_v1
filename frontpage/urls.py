from django.urls import path
from . import views

urlpatterns = [
    path("",views.FrontPage),
    path("privacy-policy/", views.PrivacyPolicy),
    path("aboutme/", views.AboutMe),
    path("contact/", views.Contact),
    path("terms-and-conditions/", views.TermsandConditions),
    path("recipe/delete/", views.DeleteRecipe),
]