from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from pymongo import MongoClient
from datetime import datetime
import sendgrid

# Create your views here.
def FrontPage(request):
    if request.method == "GET":
        if request.session.get("sid"):
            client = MongoClient("mongodb://127.0.0.1:27017") 
            db = client.hidden_recipe
            coll = db["hit_count"]            
            count = coll.find_one({})
            client.close()
            return render(request,"index/front-page.html", {"count": count["count"], "flag": "true"})
        else:
            client = MongoClient("mongodb://127.0.0.1:27017") 
            db = client.hidden_recipe
            coll = db["hit_count"]
            count = coll.find_one({})
            if count == None:
                coll.insert_one({
                    "count": 66
                })           
            else:
                if "cookieconsent_status" not in request.COOKIES:
                        coll.update_one({}, {"$set": {"count": count["count"]+1}})
            count = coll.find_one({})
            client.close()
            return render(request,"index/front-page.html", {"count": count["count"]})
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def Contact(request):
    if request.method == "POST":
        name = request.POST["name"]
        email =request.POST["email"]
        subject = request.POST["subject"]
        message = request.POST["message"]
        date = datetime.combine(datetime.utcnow().date(), datetime.min.time()) 
        client = MongoClient("mongodb://127.0.0.1:27017")
        db = client.hidden_recipe
        coll = db["newsletter"]
        coll.insert_one({
            "name": name,
            "email": email,
            "subject": subject,
            "message": message,
            "date": date
        })
        sg = sendgrid.SendGridAPIClient(apikey='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')
        data = {
        "personalizations": [
            {
            "to": [
                {
                "email": "hiddenrecipe.hr@gmail.com"
                }
            ],
            "subject": "Someone contacted you about Hidden Recipe"
            }
        ],
        "from": {
            "email": "support@hiddenrecipe.in",
            "name": "Hidden Recipe"
        },
        "content": [
            {
            "type": "text/html",
            "value": "<html><body><h3>Dear Admin, Someone contacted you. The details are below.<h3><br>Name: {}<br>Email: {}<br>Subject: {}<br>Message: {}</body></html>".format(name, email, subject, message)
            }
        ]
        }
        response = sg.client.mail.send.post(request_body=data)
        return HttpResponseRedirect('/#contact')
    if request.method == "GET":
        return HttpResponseRedirect("/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def PrivacyPolicy(request):
    if request.method == "GET":
        link = "/"
        if request.session.get("sid"):
            link = "/user/dashboard/"
        return render(request, "index/privacy-policy.html",{"link": link})
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def AboutMe(request):
    if request.method == "GET":
        link = "/"
        if request.session.get("sid"):
            link = "/user/dashboard/"
        return render(request, "index/about-me.html",{"link": link})
    else:
        return HttpResponseRedirect("https://www.youtube.com/")
def TermsandConditions(request):
    if request.method == "GET":
        link = "/"
        if request.session.get("sid"):
            link = "/user/dashboard/"
        return render(request, "index/terms-and-conditions.html",{"link": link})
    else:
        return HttpResponseRedirect("https://www.youtube.com/")

def DeleteRecipe(request):
    if request.method == "GET":
        if request.session.get("sid"):
            if request.session.get("type") == "hidden_recipe_admin":
                if request.GET.get("recipe_id"):
                    recipe_id = request.GET.get("recipe_id")
                    client = MongoClient("mongodb://127.0.0.1:27017")
                    db = client.hidden_recipe
                    coll = db["recipes"]
                    flag = coll.find_one({"recipe_id": recipe_id})
                    if flag != None:
                        coll.remove({"recipe_id": recipe_id})
                    redirect_url = "/hidden_admin/dashboard/"
                    return HttpResponseRedirect(redirect_url)
            if request.session.get("eid"):
                if request.GET.get("recipe_id"):
                    recipe_id = request.GET.get("recipe_id")
                    client = MongoClient("mongodb://127.0.0.1:27017")
                    db = client.hidden_recipe
                    coll = db["recipes"]
                    flag = coll.find_one({"recipe_id": recipe_id})
                    if flag != None:
                        coll.remove({"recipe_id": recipe_id})
                    return HttpResponseRedirect("/user/view-profile/")
            else:
                del request.session["sid"]
                return HttpResponseRedirect("/")
        else:
            return HttpResponseRedirect("/")
    else:
        return HttpResponseRedirect("https://www.youtube.com/")